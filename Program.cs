﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace AspLineBreaker
{
    public class Program
    {
        const string textToBreak = @"SELECT TOP 1
	t1.txtName 'FormInstance',
	t4.txtTitle 'Field'
FROM
	TblAdmissionsPortalFormInstance t1
	INNER JOIN TblAdmissionsPortalForm t2 ON t1.TblAdmissionsPortalFormInstanceID = t2.intFormInstanceID
	INNER JOIN TblAdmissionsPortalFormGroup t3 ON t2.txtCode = t3.txtFormCode
	INNER JOIN TblAdmissionsPortalFormGroupFields t4 on t3.txtCode = t4.txtGroupCode
	INNER JOIN TblPupilManagementCustomFields t5 ON t4.intCustomFieldID = t5.tblPupilManagementCustomFieldsID
	INNER JOIN TblUsefulLists t6 ON t5.txtListname = t6.ListType AND t4.txtDefaultValue = t6.Name
WHERE
	t6.TblUsefulListsID = 2807";


        static void Main(string[] args)
        {
            var directory = @"c:\temp";
            var aspText = BreakText(textToBreak);

            // If directory does not exist, create it. 
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            File.WriteAllText(directory + @"\asp.txt", aspText);
        }


        private static string BreakText(string sourceText)
        {
            var lines = sourceText.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
            var aspText = new StringBuilder("\"\" & _" + Environment.NewLine);
            lines.ForEach(line =>{
                aspText.Append("\"" + line + " \" & _" + Environment.NewLine);
            });
            return aspText.ToString();
        }
    }
}
